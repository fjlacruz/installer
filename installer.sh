#!/bin/bash
#Author: Francisco Javier La Cruz


echo -e "\033[33m------------------------------------------------------------------------------------------------------------------------------------------------\033[0m\n"
echo -e " \e[1;34m  
                                              _               _            _   _             
                                             (_)  _ _    ___ | |_   __ _  | | | |  ___   _ _ 
                                             | | | ' \  (_-< |  _| / _  | | | | | / -_) | '_|
                                             |_| |_||_| /__/  \__| \__,_| |_| |_| \___| |_|  
\e[0m"

# Función para mostrar el menú solo una vez
mostrar_menu() {
    echo " "
    echo -e "\033[33m------------------------------------------------------------------------------------------------------------------------------------------------\033[0m\n"
    printf "\033[33m%-10s %-30s %-40s %-40s\033[0m\n" " Option" "Template" "Framework" "Description"
    echo -e "\033[33m------------------------------------------------------------------------------------------------------------------------------------------------\033[0m\n"
    printf "%-10s %-30s %-40s %-40s\n" "  1."     "lambdats"  "serverless framework/node" "template whith serverless"
    printf "%-10s %-30s %-40s %-40s\n" "  2."     "Ejecutar comando 2" " "  "Descripción detallada del comando 2"
    printf "%-10s %-30s %-40s %-40s\n" "  3."     "Ejecutar comando 3" " "  "Descripción detallada del comando 3"
    echo " "
    printf "\033[31m%-10s %-10s %-10s\033[0m\n" "  4." "Exit" "Close installer"
    echo -e "\033[33m------------------------------------------------------------------------------------------------------------------------------------------------\033[0m\n"
    echo " "
}

# Bucle principal
mostrar_menu
while true; do
    read -p "Ingrese su opción: " opcion

    case $opcion in
        1)
            echo "Ejecutando intalacion para" opcion
            ./eject.sh "lambdats"
            break
            ;;
        2)
            echo "template 2 en contruccion..."
             ./test1.sh "template2"
            break
            ;;
        3)
            echo "template 3 en contruccion..."
            ./test1.sh "template3"
            break
            ;;
        4)
            echo "Saliendo..."
            exit 0
            ;;
        *)
            echo -e "\033[31mOpción no válida ❌\033[0m"
            ;;
    esac
done
