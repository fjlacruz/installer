#!/bin/bash
#Author: Francisco Javier La Cruz


# echo -e  " \e[1;34m******************************************************************************************\e[0m "
# echo "   
#                       _               _            _   _             
#                      (_)  _ _    ___ | |_   __ _  | | | |  ___   _ _ 
#                      | | | ' \  (_-< |  _| / _  | | | | | / -_) | '_|
#                      |_| |_||_| /__/  \__| \__,_| |_| |_| \___| |_|  
# "
# echo -e  " \e[1;34m******************************************************************************************\e[0m "
echo ""
echo "+----------------------------------------------------------------------------------------+"
echo "|                   Proceso de instalación de tamplate para Backend                      |"
echo "+----------------------------------------------------------------------------------------+"
echo ""

######################################################
#### Valiadando instalacionde requisitos mínimos #####
######################################################
check_installed() {
    for command_name in "$@"; do
        if command -v "$command_name" > /dev/null 2>&1; then
            version=$("$command_name" --version | head -n 1 | awk '{print $NF}')
            printf "Verificando %-10s... \033[32m✅ Instalado\033[0m (Versión: %s)\n" "$command_name" "$version"
        else
            printf "Verificando %-10s... \033[31m❌ No instalado\033[0m\n" "$command_name"
            exit 1
        fi
    done
}
# Lista de comandos a validar
commands_to_check=("git" "glab" "curl" "node" "npm")
#referencia cli gitlab
#https://docs.gitlab.com/ee/editor_extensions/gitlab_cli/#install-the-cli
#https://github.com/profclems/glab/releases
# Muestra un mensaje de loading
echo  -e "\033[1;33mPASO 1/3: Realizando validaciones\e[0m"
# Llama a la función de validación con la lista de comandos
check_installed "${commands_to_check[@]}"

#######################################################
################## Clonando template ##################
#######################################################
#valida si existe el archivo de configuracion de variables
if [ -f config.env ]; then
    source config.env
else
    echo "Error: el archivo config.env no existe."
    exit 1
fi

# Uso de las variables de gitlab
GITLAB_API_TOKEN=$GITLAB_API_TOKEN
GITLAB_API_BASE=$GITLAB_API_BASE
USER=$USER
echo ""
echo -e "\033[1;33mPASO 2/3: Construyendo tempalte\e[0m"
echo "¿Tiene acceso al repositorio de GitLab? (s/n)"
# Inicializa la variable de respuesta
respuesta=""
# Bucle para seguir solicitando la entrada hasta que se reciba una respuesta válida
while [[ ! $respuesta =~ ^[Ss]$ ]] && [[ ! $respuesta =~ ^[Nn]$ ]]; do
    read -p "Por favor, ingrese 's' para sí o 'n' para no: " respuesta
    # Si la respuesta es 'n' o 'N', asume que el usuario no tiene acceso y termina el script
    if [[ $respuesta == [Nn]* ]]; then
        echo "Por favor, verifique su acceso al repositorio de GitLab."
        exit 1
    fi
done
cd ../..
echo "El template sera descargado en el directorio: $(pwd)"
echo "..."
echo "comenzando descarga del template...!!!"
git clone -b dev https://gitlab.com/$USER/$1.git  
echo -e "\e[0;32mdescarga finalizada ✅ \e[0m"
cd $1  
rm -rf .git

# Inicializa la variable de repositorio
repositorio=""
# Bucle para seguir solicitando la entrada hasta que se reciba un valor
while [[ -z $repositorio ]]; do
    read -p "Nombre del repositorio: " repositorio
    # Si el usuario no introduce nada, se le solicita que lo haga de nuevo
    if [[ -z $repositorio ]]; then
        echo "El campo es obligatorio. Por favor, introduzca el nombre del repositorio."
    fi
done

#valida si existe una carpeta con el nombre del repositorio en local
cd ..
repo="$(pwd)"
# Nombre de la carpeta a verificar
carpeta=$repositorio
# Verificar si la carpeta existe dentro del directorio
if [ -d "$repo/$carpeta" ]; then
    echo -e "\e[0;31mLa carpeta '$carpeta' existe dentro del directorio local '$repo' ❌\e[0m "
else
    nombre_actual="$1"
    nuevo_nombre=$repositorio 
    mv "$nombre_actual" "$nuevo_nombre"
    if [ $? -eq 0 ]; then
    echo -e "\e[0;32mSe cambio el nombre de la carpeta del proyecto local a: $repositorio ✅ \e[0m"
    chmod 777 "$repositorio"
    cd $repositorio
else
    echo -e "\e[0;31mHubo un error al cambiar el nombre de la carpeta. ❌\e[0m"
fi
fi


#valida si existe un repositorio en gitlab
RESPONSE=$(curl -s -H "PRIVATE-TOKEN: $GITLAB_API_TOKEN" "$GITLAB_API_BASE")

# Define el nombre del repositorio que deseas verificar
REPOSITORY_NAME="$USER/$repositorio"

# Realiza una solicitud GET al API de GitLab para obtener información sobre el repositorio específico
RESPONSE=$(curl -s -o /dev/null -w "%{http_code}" -H "PRIVATE-TOKEN: $GITLAB_API_TOKEN" "$GITLAB_API_BASE/$REPOSITORY_NAME")

# Verifica el código de estado de la respuesta
if [ "$RESPONSE" -eq 200 ]; then
    echo -e "\e[0;31mEl repositorio $REPOSITORY_NAME existe. ❌\e[0m"
else
    # Crea el repositorio
git  init --initial-branch=main
glab repo create
echo -e "\e[0;32mRepositorio creado con éxito ✅ \e[0m"
git remote add origin git@gitlab.com:$USER/$repositorio.git
git add .
git commit -m "Initial commit"
git push --set-upstream origin main
fi
sleep 3
echo "................."
#######################################################
######### configuraciones de branch en gitlab #########
#######################################################
echo ""
echo -e "\033[1;33mPASO 3/3: Configuraciones de branch en gitlab\e[0m"
#repositorio="ms-6"

# Array de nombres de las ramas
branches=("PROD" "QA" "DEV")

# Iterar sobre cada nombre de rama y realizar las operaciones necesarias
for branch_name in "${branches[@]}"; do
    # Crear la nueva rama localmente
    git checkout -b $branch_name

    # Empujar la nueva rama al repositorio remoto
    git push -u origin $branch_name

    # Crear la solicitud de fusión (merge request) en GitLab
    glab mr create -b $branch_name
done
echo -e "\e[0;32mDirijase al directorio: $(pwd) donde se encuentra el proyecto \e[0m"
echo -e "\e[0;32mSe abrirá una venta con el proyecto en gitlab \e[0m"
sleep 5
glab repo view $USER/$repositorio --web