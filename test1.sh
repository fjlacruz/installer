#!/bin/bash
Author: Francisco Javier La Cruz


echo -e  " \e[1;34m******************************************************************************************\e[0m "
echo "   
                      _               _            _   _             
                     (_)  _ _    ___ | |_   __ _  | | | |  ___   _ _ 
                     | | | ' \  (_-< |  _| / _  | | | | | / -_) | '_|
                     |_| |_||_| /__/  \__| \__,_| |_| |_| \___| |_|  
"
echo -e  " \e[1;34m******************************************************************************************\e[0m "
echo ""
echo "+----------------------------------------------------------------------------------------+"
echo "|                   Proceso de instalación de tamplate para Backend                      |"
echo "+----------------------------------------------------------------------------------------+"
echo ""


#!/bin/bash

# Verificar si se ha proporcionado un argumento
if [ $# -eq 0 ]; then
    echo "No se proporcionó ningún argumento."
else
    # Imprimir el argumento proporcionado
    echo "El argumento proporcionado es: $1"
fi
